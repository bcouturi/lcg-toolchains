###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
include(${CMAKE_CURRENT_LIST_DIR}/common.cmake)

list(APPEND CMAKE_PREFIX_PATH /cvmfs/projects.cern.ch/intelsw/psxe/linux/x86_64/2019/vtune_amplifier_2019.4.0.597835)
