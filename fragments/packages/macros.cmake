macro(_init_from_env)
    file(TO_CMAKE_PATH "$ENV{PATH}" path)
    file(TO_CMAKE_PATH "$ENV{LD_LIBRARY_PATH}" ld_library_path)
    file(TO_CMAKE_PATH "$ENV{PYTHONPATH}" pythonpath)
    file(TO_CMAKE_PATH "$ENV{ROOT_INCLUDE_PATH}" root_include_path)
    set(pythonpath) # empty the PYTHONPATH
endmacro()

macro(_add_lcg_entry entry)
    list(APPEND CMAKE_PREFIX_PATH ${entry})
    if(EXISTS ${entry}/bin)
        list(PREPEND path ${entry}/bin)
    endif()
    foreach(_suffix IN ITEMS lib lib64)
        if(EXISTS ${entry}/${_suffix})
            file(GLOB inner_files "${entry}/${_suffix}/*.so*")
            if(inner_files)
                list(PREPEND ld_library_path ${entry}/${_suffix})
            endif()
            file(GLOB inner_files "${entry}/${_suffix}/*.py")
            if(inner_files)
                list(PREPEND pythonpath ${entry}/${_suffix})
            endif()
            if(EXISTS "${entry}/${_suffix}/python${_python_version}/site-packages")
                list(PREPEND pythonpath ${entry}/${_suffix}/python${_python_version}/site-packages)
            endif()
        endif()
    endforeach()
    if(EXISTS ${entry}/include)
        list(PREPEND root_include_path ${entry}/include)
    endif()
endmacro()

macro(_update_env)
    # file(TO_NATIVE_PATH) does not transform ; to :
    string(REPLACE ";" ":" path "${path}")
    set(ENV{PATH} "${path}")
    string(REPLACE ";" ":" ld_library_path "${ld_library_path}")
    set(ENV{LD_LIBRARY_PATH} "${ld_library_path}")
    string(REPLACE ";" ":" pythonpath "${pythonpath}")
    set(ENV{PYTHONPATH} "${pythonpath}")
    string(REPLACE ";" ":" root_include_path "${root_include_path}")
    set(ENV{ROOT_INCLUDE_PATH} "${root_include_path}")
endmacro()

macro(_fix_pkgconfig_search)
    # tuning of cmake delegation to pkg-config (https://cmake.org/cmake/help/latest/module/FindPkgConfig.html)
    # if we let FindPkgConfig rely on CMAKE_PREFIX_PATH we will miss either lib or lib64 subdirs
    # - make sure pkg-config uses CMAKE_PREFIX_PATH
    set(PKG_CONFIG_USE_CMAKE_PREFIX_PATH YES)
    # - when crosscompiling, CMake ignores /usr/lib64/pkgconfig, but this is kind of a hybrid
    if(EXISTS /usr/lib64/pkgconfig AND NOT EXISTS ${CMAKE_BINARY_DIR}/toolchain/lib/pkgconfig)
        file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/toolchain/lib/pkgconfig)
        file(GLOB _system_pc_files RELATIVE /usr/lib64/pkgconfig "/usr/lib64/pkgconfig/*.pc")
        foreach(_system_pc_file IN LISTS _system_pc_files)
            file(CREATE_LINK /usr/lib64/pkgconfig/${_system_pc_file} ${CMAKE_BINARY_DIR}/toolchain/lib/pkgconfig/${_system_pc_file} SYMBOLIC)
        endforeach()
    endif()
    if(EXISTS ${CMAKE_BINARY_DIR}/toolchain/lib/pkgconfig)
        list(APPEND CMAKE_PREFIX_PATH ${CMAKE_BINARY_DIR}/toolchain)
    endif()
endmacro()

macro(_set_pythonhome pythonhome)
    # PYTHONHOME needed by gdb
    set(ENV{PYTHONHOME} "${pythonhome}")

    # prevent find_package(Python) too look into the path
    set(Python_ROOT_DIR "${pythonhome}")
    set(Python2_ROOT_DIR "${pythonhome}")
    set(Python3_ROOT_DIR "${pythonhome}")
endmacro()
