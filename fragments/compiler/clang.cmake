###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# This file contains what is needed to use Clang
include_guard(GLOBAL)

# Define compiler cache variables
set(CMAKE_CXX_COMPILER ${CMAKE_BINARY_DIR}/toolchain/clang++ CACHE FILEPATH "Path to C++ compiler")
set(CMAKE_C_COMPILER ${CMAKE_BINARY_DIR}/toolchain/clang CACHE FILEPATH "Path to C compiler")
mark_as_advanced(CMAKE_CXX_COMPILER CMAKE_C_COMPILER)

# Import GCC toolchain
if(NOT LCG_CLANG_GCC_TOOLCHAIN STREQUAL "")
  set(_LCG_CLANG_VERSION "${LCG_COMPILER_VERSION}")
  include(${CMAKE_CURRENT_LIST_DIR}/${LCG_HOST}-gcc${LCG_CLANG_GCC_TOOLCHAIN}.cmake)
  string(APPEND CMAKE_C_FLAGS " --gcc-toolchain=${COMPILER_ROOT}")
  string(APPEND CMAKE_CXX_FLAGS " --gcc-toolchain=${COMPILER_ROOT}")
  set(LCG_COMPILER_VERSION "${_LCG_CLANG_VERSION}")
  unset(_LCG_CLANG_VERSION)
endif()
string(REGEX REPLACE "-.*$" "" LCG_COMPILER_BASE_VERSION "${LCG_COMPILER_VERSION}")

# Set the compiler
set(COMPILER_ROOT ${LCG_releases_base}/clang/${LCG_COMPILER_VERSION}/${LCG_HOST})

set(ENV{PATH} "${COMPILER_ROOT}/bin:$ENV{PATH}")
# FIXME: strip existing gcc|llvm|clang entries from LD_LIBRARY_PATH
set(ENV{LD_LIBRARY_PATH} "${COMPILER_ROOT}/lib:$ENV{LD_LIBRARY_PATH}")

_dedup_env_path(PATH)
_dedup_env_path(LD_LIBRARY_PATH)
_dedup_env_path(ROOT_INCLUDE_PATH)

# generate wrapper scripts for the compilers
foreach(_cmd IN ITEMS clang++ clang)
  message(STATUS "Writing ${CMAKE_BINARY_DIR}/toolchain/${_cmd} for ${CMAKE_SOURCE_DIR}")
  file(WRITE "${CMAKE_BINARY_DIR}/toolchain/${_cmd}"
"#!/bin/sh
export PATH=$ENV{PATH}
export LD_LIBRARY_PATH=$ENV{LD_LIBRARY_PATH}
exec ${COMPILER_ROOT}/bin/${_cmd} \"\$@\"
")
  execute_process(COMMAND chmod a+x ${CMAKE_BINARY_DIR}/toolchain/${_cmd})
  execute_process(COMMAND touch --reference=${COMPILER_ROOT}/bin/${_cmd} ${CMAKE_BINARY_DIR}/toolchain/${_cmd})
endforeach()
