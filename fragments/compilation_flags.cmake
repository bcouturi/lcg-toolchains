cmake_minimum_required(VERSION 3.15)

# Options to fine tune the compilation flags
option(LCG_DIAGNOSTICS_COLOR "force colors in compiler diagnostics" OFF)

# Set build type
if(NOT CMAKE_BUILD_TYPE)
  if(LCG_OPTIMIZATION STREQUAL "opt")
    set(CMAKE_BUILD_TYPE Release)
  elseif(LCG_OPTIMIZATION STREQUAL "dbg")
    set(CMAKE_BUILD_TYPE Debug)
  endif()
  set(CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE} CACHE STRING
    "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel ...")
endif()

# Set common flags
set(_common_flags)
# - architecture
#   - use gcc convention (e.g. x86-64 instead of x86_64)
string(REPLACE "_" "-" LCG_ARCHITECTURE "${LCG_ARCHITECTURE}")
#   - custom aliases
string(REPLACE "vecwid256" "prefer-vector-width=256" LCG_ARCHITECTURE "${LCG_ARCHITECTURE}")
#   - extract main arch and options
if(LCG_ARCHITECTURE MATCHES "\\+")
    string(REGEX MATCHALL "[^+]+" LCG_ARCHITECTURE_OPTIONS "${LCG_ARCHITECTURE}")
    # the first chunk is the actual architecture
    list(POP_FRONT LCG_ARCHITECTURE_OPTIONS LCG_ARCHITECTURE)
    list(TRANSFORM LCG_ARCHITECTURE_OPTIONS PREPEND "-m")
else()
    # no architecture option
    set(LCG_ARCHITECTURE_OPTIONS)
endif()
list(PREPEND LCG_ARCHITECTURE_OPTIONS "-march=${LCG_ARCHITECTURE}")
list(JOIN LCG_ARCHITECTURE_OPTIONS " " LCG_ARCHITECTURE_OPTIONS)

string(APPEND _common_flags " -fmessage-length=0 -pipe -pedantic")

set(_common_warnings all extra error=return-type)
set(_CXX_warnings write-strings pointer-arith overloaded-virtual non-virtual-dtor)
set(_C_warnings write-strings pointer-arith)

if(LCG_COMPILER MATCHES "^gcc.*$")
  list(APPEND _CXX_warnings suggest-override)
endif()

foreach(_lang IN ITEMS CXX C Fortran)
    list(PREPEND _${_lang}_warnings ${_common_warnings})
    list(TRANSFORM _${_lang}_warnings PREPEND "-W")
    list(JOIN _${_lang}_warnings " " _${_lang}_warnings)

    set(CMAKE_${_lang}_FLAGS
        "${CMAKE_${_lang}_FLAGS} ${LCG_ARCHITECTURE_OPTIONS} ${_common_flags} ${_${_lang}_warnings}"
        CACHE STRING "Flags used by the compiler during all build types.")
endforeach()
