#!/usr/bin/env python3
###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Script to generate the files required for a new LCG release.
"""

import os
import sys
import logging
from subprocess import check_call

LCG_META_INFO_ROOT = "https://lcgpackages.web.cern.ch/lcgpackages/lcg/meta/"


def parse_lcg_info_line(line):
    """
    Parse lines like ' KEY1: value1, KEY2: value2.1,value2.b,' into a dictionary

    >>> parse_lcg_info_line(' KEY1: value1, KEY2: value2.1,value2.b,')
    {'KEY1': 'value1', 'KEY2': 'value2.1,value2.b'}
    """
    import re

    exp = re.compile(r",? ([A-Z_][A-Z0-9_]*): ")

    pos = 0
    key = None
    data = {}
    m = exp.search(line)
    while m:
        if key and pos:
            data[key] = line[pos : pos + m.start()]
        key = m.group(1)
        pos += m.end()
        m = exp.search(line[pos:])
    if key and pos:
        data[key] = line[pos:].rstrip().rstrip(",")
    return data


def parse_lcg_info(lines):
    """
    Parse the lines of an LCG release metadata file (as an iterable object).
    """
    data = {"compiler": None, "packages": {}}
    for line in lines:
        if line.lstrip().startswith("#"):
            continue
        line = parse_lcg_info_line(line)
        if line.get("COMPILER"):
            data["compiler"] = line["COMPILER"]
        else:
            print(line)
        data["packages"][line["NAME"]] = {
            "version": line["VERSION"],
            "directory": line["DIRECTORY"],
            "hash": line["HASH"],
            "platform": line["PLATFORM"],
            "dependencies": line["DEPENDS"].split(",") if "DEPENDS" in line else [],
        }
        if line["NAME"] != line["DESTINATION"]:
            print(line)
    return data


def get_lcg_info(version: str, platform: str):
    """
    Return the list of packages for a given LCG version and platform.
    """
    from urllib.request import urlopen

    url = "{base}LCG_{version}_{platform}.txt".format(
        base=LCG_META_INFO_ROOT, version=version, platform=platform
    )
    logging.debug("getting info from %s", url)

    return parse_lcg_info(l.decode("utf-8") for l in urlopen(url))


def make_tree(tree, root=os.path.curdir, do_git=True):
    from collections.abc import Mapping

    for key in tree:
        path = os.path.join(root, key)
        if isinstance(tree[key], Mapping):
            if not os.path.isdir(path):
                os.makedirs(path)
            logging.debug("entering %s", path)
            make_tree(tree[key], path, do_git)
        elif isinstance(tree[key], Symlink):
            if os.path.exists(path):
                os.remove(path)
            os.symlink(str(tree[key]), path)
            if do_git:
                check_call(["git", "add", key], cwd=root)
        else:
            logging.debug("writing  %s", path)
            with open(path, "w") as f:
                f.write(_header())
                f.write(tree[key])
            if do_git:
                check_call(["git", "add", key], cwd=root)


def _header():
    from datetime import date

    return HEADER_TEMPLATE.format(year=date.today().year)


def _entry_point(version, platform):
    if "=" in platform:
        platform, lcg_platform = platform.split("=", 1)
        architecture = platform.split("-")[0]
        return TOOLCHAIN_ALIAS.format(
            architecture=architecture, lcg_platform=lcg_platform
        )
    else:
        return TOOLCHAIN_ENTRY_POINT.format(version=version, platform=platform)


def _compiler(system, compiler):
    family, version = compiler.split()
    if family == "GNU":
        name = "gcc"
    elif family == "Clang":
        name = "clang"
    else:
        raise ValueError("unknown compiler {!r}".format(compiler))

    return Symlink("{}-{}{}.cmake".format(system.rsplit("-", 1)[0], name, version))


def _packages(lcg_info):
    data = [
        "include(${CMAKE_CURRENT_LIST_DIR}/macros.cmake)",
        "_init_from_env()",
        "set(_python_version {}.{})".format(
            *lcg_info["packages"]["Python"]["version"].split(".")
        ),
    ]

    data.extend(
        '_add_lcg_entry("{}")'.format(LCG_PACKAGE_PATH.format(**info))
        for name, info in lcg_info["packages"].items()
        if name.lower() not in ("ninja", "cmake", "gaudi")
    )
    data.append("_update_env()")
    data.append("_fix_pkgconfig_search()")
    if "Python" in lcg_info["packages"]:
        data.append(
            '_set_pythonhome("{}")'.format(
                LCG_PACKAGE_PATH.format(**lcg_info["packages"]["Python"])
            )
        )
    data.append(INTELAMPLIFIER_ENTRY)
    return "\n".join(data)


class Symlink:
    def __init__(self, target: str):
        self.target = target

    def __str__(self):
        return self.target


def main():
    from argparse import ArgumentParser

    parser = ArgumentParser(
        description="Create required files for a given LCG release and platform"
    )

    parser.add_argument("version", help="version of LCG to generate")
    parser.add_argument("platform", nargs="+", help="platforms to generate")
    parser.add_argument("--no-git", action="store_true", help="do not commit to git")

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG)

    data = {
        platform: get_lcg_info(args.version, platform)
        for platform in set(p.split("=", 1)[-1] for p in args.platform)
    }
    # subplatform string ignoring the optimization level
    systems = {
        "-".join(platform.split("-")[:-1]): data[platform]["compiler"]
        for platform in data
    }
    # dictionary of the values to use in the templates
    # this is a dictionary of directories and files to be created
    # keys corresponding to dictionaries map to directories, while keys
    # mapping to string represent files
    # - first we add the entry points
    output = {
        "LCG_{}".format(args.version): {
            "{}.cmake".format(platform.split("=", 1)[0]): _entry_point(
                args.version, platform
            )
            for platform in args.platform
        },
        "fragments": {
            "compiler": {
                "LCG_{version}-{system}.cmake".format(
                    version=args.version, system=system
                ): _compiler(system, compiler)
                for system, compiler in systems.items()
            },
            "packages": {
                "LCG_{version}-{platform}.cmake".format(
                    version=args.version, platform=platform
                ): _packages(data[platform])
                for platform in data
            },
        },
    }
    make_tree(output, do_git=not args.no_git)
    if not args.no_git:
        check_call(
            [
                "git",
                "commit",
                "-m",
                "Added LCG {version}\n\ncommand:\n    {command}".format(
                    version=args.version, command=" ".join(sys.argv)
                ),
            ]
        )

    return 0


# --- Resources ---
HEADER_TEMPLATE = """###############################################################################
# (c) Copyright {year:4} CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# DO NOT EDIT: File generated by new_lcg_version.py

"""

TOOLCHAIN_ENTRY_POINT = """# Make sure the toolchain is included only once
# (to avoid double changes to the environment)
include_guard(GLOBAL)

cmake_policy(PUSH)
cmake_policy(SET CMP0007 NEW)

message(STATUS "Entering ${{CMAKE_CURRENT_LIST_FILE}}")
if(NOT DEFINED LCG_VERSION)
  get_filename_component(LCG_VERSION "${{CMAKE_CURRENT_LIST_DIR}}" NAME)
endif()
if(NOT DEFINED LCG_PLATFORM)
  get_filename_component(LCG_PLATFORM "${{CMAKE_CURRENT_LIST_FILE}}" NAME_WE)
endif()
string(REPLACE "-" ";" _platform_bits "${{LCG_PLATFORM}}")
if(NOT DEFINED LCG_ARCHITECTURE)
  list(GET _platform_bits 0 LCG_ARCHITECTURE)
endif()
if(NOT DEFINED LCG_OS)
  list(GET _platform_bits 1 LCG_OS)
endif()
if(NOT DEFINED LCG_COMPILER)
  list(GET _platform_bits 2 LCG_COMPILER)
endif()
if(NOT DEFINED LCG_OPTIMIZATION)
  list(GET _platform_bits 3 LCG_OPTIMIZATION)
endif()

string(REGEX REPLACE "-[^-]+\$" "" LCG_SYSTEM "${{LCG_PLATFORM}}")
string(REGEX REPLACE "-[^-]+\$" "" LCG_HOST "${{LCG_SYSTEM}}")

foreach(bit IN ITEMS VERSION ARCHITECTURE OS COMPILER OPTIMIZATION PLATFORM SYSTEM)
    message(STATUS "LCG_${{bit}} -> ${{LCG_${{bit}}}}")
endforeach()

set(LCG_releases_base "/cvmfs/lhcb.cern.ch/lib/lcg/releases" CACHE PATH "Where to look for LCG releases")
set(FRAGMENTS_DIR ${{CMAKE_CURRENT_LIST_DIR}}/../fragments)

#[[---.rst
Helper function to remove duplicated from a PATH-like variables
#---]]
macro(_dedup_env_path name)
  string(REPLACE ":" ";" _${{name}}_tmp "$ENV{{${{name}}}}")
  list(REMOVE_DUPLICATES _${{name}}_tmp)
  list(FILTER _${{name}}_tmp EXCLUDE REGEX "^$")
  string(REPLACE ";" ":" _${{name}}_tmp "${{_${{name}}_tmp}}")
  set(ENV{{${{name}}}} "${{_${{name}}_tmp}}")
  unset(_${{name}}_tmp)
endmacro()

include(${{FRAGMENTS_DIR}}/compiler/${{LCG_VERSION}}-${{LCG_SYSTEM}}.cmake)
include(${{FRAGMENTS_DIR}}/packages/${{LCG_VERSION}}-${{LCG_PLATFORM}}.cmake)

_dedup_env_path(PATH)
_dedup_env_path(LD_LIBRARY_PATH)
_dedup_env_path(ROOT_INCLUDE_PATH)

message(STATUS "Writing ${{CMAKE_BINARY_DIR}}/toolchain/wrapper for ${{CMAKE_SOURCE_DIR}}")
file(WRITE ${{CMAKE_BINARY_DIR}}/toolchain/wrapper
"#!/bin/sh -e
export PATH=$ENV{{PATH}}
export LD_LIBRARY_PATH=$ENV{{LD_LIBRARY_PATH}}
export PYTHONPATH=$ENV{{PYTHONPATH}}
export PYTHONHOME=$ENV{{PYTHONHOME}}
export ROOT_INCLUDE_PATH=$ENV{{ROOT_INCLUDE_PATH}}

exec \\"\$@\\"
")
execute_process(COMMAND chmod a+x ${{CMAKE_BINARY_DIR}}/toolchain/wrapper)

include(${{FRAGMENTS_DIR}}/compilation_flags.cmake)

set(CMAKE_SYSTEM_NAME ${{CMAKE_HOST_SYSTEM_NAME}})
set(CMAKE_SYSTEM_PROCESSOR ${{CMAKE_HOST_SYSTEM_PROCESSOR}})
set(CMAKE_CROSSCOMPILING_EMULATOR ${{CMAKE_BINARY_DIR}}/toolchain/wrapper)

# Allow definition of rule wrappers from cache variables
foreach(_action IN ITEMS COMPILE LINK CUSTOM)
  if(DEFINED CMAKE_RULE_LAUNCH_${{_action}})
    set_property(GLOBAL PROPERTY RULE_LAUNCH_${{_action}} "${{CMAKE_RULE_LAUNCH_${{_action}}}}")
  endif()
endforeach()

cmake_policy(POP)
"""

TOOLCHAIN_ALIAS = """# Make sure the toolchain is included only once
# (to avoid double changes to the environment)
include_guard(GLOBAL)

set(LCG_ARCHITECTURE {architecture})
include(${{CMAKE_CURRENT_LIST_DIR}}/{lcg_platform}.cmake)
"""

LCG_PACKAGE_PATH = "${{LCG_releases_base}}/{directory}/{version}-{hash}/{platform}"

INTELAMPLIFIER_ROOT = "/cvmfs/projects.cern.ch/intelsw/psxe/linux/x86_64/2019/vtune_amplifier_2019.4.0.597835"

INTELAMPLIFIER_ENTRY = """
set(INTELAMPLIFIER_ROOT {})
if(EXISTS "${{INTELAMPLIFIER_ROOT}}")
  list(APPEND CMAKE_PREFIX_PATH "${{INTELAMPLIFIER_ROOT}}")
  set(GAUDI_USE_INTELAMPLIFIER TRUE CACHE BOOL "enable IntelAmplifier based profiler in Gaudi")
else()
  message(WARNING "IntelAplifier directory ${{INTELAMPLIFIER_ROOT}} not found, turning off profiler")
  set(GAUDI_USE_INTELAMPLIFIER FALSE CACHE BOOL "enable IntelAmplifier based profiler in Gaudi")
endif()
""".format(
    INTELAMPLIFIER_ROOT
)


if __name__ == "__main__":
    sys.exit(main())
