# LCG Toolchains

[CMake][] [toolchain files][CMake toolchains] to build a standard CMake project on top of LCG releases.

## Usage
To enable a toolchain it's enough to add the option `-DCMAKE_TOOLCHAIN_FILE=path/to/the/toolchain.cmake` to the call to `cmake`, e.g.:
```
cmake -S . -B build -DCMAKE_TOOLCHAIN_FILE=/opt/lcg-toolchains/LCG_96b/x86_64-centos7-gcc9-opt.cmake
```

The toolchain code will create a few helper scripts in the build tree, under the directory `${CMAKE_BINARY_DIR}/toolchain`:
- wrappers for the compilers
- generic `wrapper` command
- lib directory for `pkgconfig` workaround

## Layout
The toolchain files are organized in one directory per supported LCG version
plus one directory ([`fragments`](fragments)) with the implementation details.

The LCG version directory contains, one symlink to a CMake fragment common to
all platforms ([`fragments/main.cmake`](fragments/main.cmake)) for each LCG binary tag,
and one file for each special platform used by LHCb, which delegates to the corresponding
LCG binary tag.

The main fragment then delegates to LCG version specific files for the
configuration of the compiler, with the details in one file per specific version of
the compiler. Then a file from the directory [`fragments/packages`](fragments/packages)
is included for the specific settings for the external packages.

The special file [`fragments/compilation_flags.cmake`](fragments/compilation_flags.cmake)
is then included to set the LHCb specific compilation flags (e.g. architecture flags,
warnings settings, etc.).

## Customize the toolchains
By default the value of `LCG_PLATFORM` (from the name of the CMake file) is used to set
`LCG_ARCHITECTURE` and `LCG_OPTIMIZATION`, which are used to tune the compilation flags,
but they can be overridden from the command line, for example to build for a custom
architecture, not officially supported:
```
cmake -S . -B build -DLCG_ARCHITECTURE=skylake -DCMAKE_TOOLCHAIN_FILE=/opt/lcg-toolchains/LCG_96b/x86_64-centos7-gcc9-opt.cmake
```


[CMake]: https://cmake.org
[CMake toolchains]: https://cmake.org/cmake/help/latest/manual/cmake-toolchains.7.html
